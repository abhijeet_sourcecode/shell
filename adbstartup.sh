#!/bin/bash

source /home/test1080devicefarm/scripts/device.sh

# =========== Enter Path Names ================= #

logfile=
AWSIP=
AWSKEY=
LOCALADB=
AWSADB=
PYTHONCODE=
AWSNODE=
NODESCRIPT=
PATHTOCSV=
PATHTOMONITOR=
AWSSTARTSCRIPT=


# ============================================== #
timestamp(){
	date +"%s"
}
echo "0" > $PATHTOMONITOR

declare -A connectionStat
for key in ${!allDevices[@]};do
	connectionStat[X$key]=0
done

echo "==============================" >> $logfile
timestamp >> $logfile

for k in ${!allDevices[@]};do
	echo ${k} "-->" ${allDevices[${k}]} >> $logfile
done


echo "killing adb server on amazon" >> $logfile
ssh -i $AWSKEY ubuntu@$AWSIP $AWSADB kill-server >> $logfile 

echo "killing all screens on amazon" >> $logfile
ssh -i $AWSKEY ubuntu@$AWSIP killall screen >> $logfile 

echo "killing adb server on local" >> $logfile
$LOCALADB kill-server >> $logfile

echo "killing all screens" >> $logfile
killall screen >> $logfile

echo "making a screen for tunnel" >> $logfile
screen -d -m -S Tunnel >> $logfile

echo "tunneling port 5037" >> $logfile
#screen -S Tunnel -X stuff "ssh -i $AWSKEY -N -R 5037:localhost:5037 ubuntu@$AWSIP\n"  >> $logfile
screen -S Tunnel -X stuff "autossh -M 55000 -i $AWSKEY -N -R 5037:localhost:5037 ubuntu@$AWSIP\n"  >> $logfile

echo "starting adb server on local" >> $logfile
$LOCALADB start-server  >> $logfile

echo "waiting for adb to start properly (sleep 5 seconds)" >> $logfile
sleep 5 

#-------------------- check devices -----------------
export IFS=";"
devices=$($LOCALADB devices|grep -v " device"|tr "\t" "\n"|grep -v "device"|tr "\n" ";")
i=0
devicelist=()
for device in $devices;do
               i=$((i+1))
		if [ -n "$device" ];
		then
               		echo " adb returned $device" >> $logfile
               		devicelist+=(X$device)
		fi
done

deviceCount=0
for device in ${!allDevices[@]};do
	dlen=${#device}
	dbkey=${device:1:$dlen}
	flag=false
	for dev in ${devicelist[@]};do
		#echo "compare $device to $dev"	
		if [[ $device ==  $dev ]]
		then	
			flag=true
			echo -e "${allDevices[${device}]} is connected!" >> $logfile

			echo "making screen for device $dev" >> $logfile
			screen -d -m -S $device 
			
			echo "forwarding to port" >> $logfile
			#screen -S $device -X stuff "ssh -i $AWSKEY -N -R ${Ports[$device]}:localhost:${Ports[$device]} ubuntu@$AWSIP\n"
			screen -S $device -X stuff "autossh -M ${autoPorts[$device]} -i $AWSKEY -N -R ${Ports[$device]}:localhost:${Ports[$device]} ubuntu@$AWSIP\n"
			connectionStat[$device]=1
			deviceCount=$((deviceCount+1))
			echo "updating $dbkey status in db to 1" >> $logfile
			python $PYTHONCODE $dbkey 1
			break
		fi
	done
	
	if  ! $flag 
	then	
		echo -e "${allDevices[${device}]} not connected!" >> $logfile
		echo "updating $dbkey status in db to 0" >> $logfile
		python $PYTHONCODE $dbkey 0 
	fi	
done



echo "$deviceCount device(s) are connected at the moment" >> $logfile
#-------------------- check devices finished -----------------
#
#-------------------- updating  devices on DB  -----------------

#for key in ${!connectionStat[@]};do
#	echo "updating $key ${allDevices[$key]} to ${connectionStat[$key]}" >> $logfile
#	python $PYTHONCODE $key ${connectionStat[$key]}
#done	

#-------------------- updating  devices on DB finished -----------------

echo "checking devices on local" >> $logfile
$LOCALADB devices  >> $logfile

echo "starting adb server on amazon" >> $logfile
ssh -i $AWSKEY ubuntu@$AWSIP $AWSADB start-server >> $logfile

echo "checking adb devices on amazon" >> $logfile
ssh -i $AWSKEY ubuntu@$AWSIP $AWSADB devices >> $logfile

#-------------------- starting node -----------------
nodename="Node"
nodename+=$(date -d "today" +"%Y%m%d%H%M") 

echo "making a screen for $nodename" >> $logfile
screen -d -m -S $nodename >> $logfile

echo "starting node server on amazon" >> $logfile
screen -S $nodename -X stuff "ssh -i $AWSKEY ubuntu@$AWSIP $AWSNODE $NODESCRIPT\n" >> $logfile

#-------------------- start scheduler, issue -----------------
echo "starting processes on amazon" >> $logfile
ssh -i $AWSKEY ubuntu@$AWSIP source $AWSSTARTSCRIPT >> $logfile
