#!/bin/bash

source /home/test1080devicefarm/scripts/device.sh


# =========== Enter Path Names ================= #

logfile=
AWSIP=
AWSKEY=
LOCALADB=
AWSADB=
PYTHONCODE=
AWSNODE=
NODESCRIPT=
PATHTOCSV=
PATHTOMONITOR=
AWSSTARTSCRIPT=


# ============================================== #


timestamp(){
	date +"%s"
}



echo '==========================' >> $logfile

timestamp >> $logfile

echo "usb device removed: $1" >> $logfile

for k in ${!allDevices[@]};do
	echo ${k} "-->" ${allDevices[${k}]}
done


echo "getting running screens"  >> $logfile
# get all running screens
scrcmd=$(ps auxw|grep -i screen|grep -v grep|awk '{print $15}')
allscr=()
export IFS=" "
while read -r line; do
   	allscr+=("$line")
done <<< $scrcmd

# get the screens that are forwarding either one of the configured devices 
scr=()
for d in "${!allDevices[@]}" 
do : 
	for a in "${allscr[@]}" 
	do : 
		#echo "compare ${d} with $a"
		if [ "$d" == "$a" ];
		then
			scr+=($a)
			echo "found screen" $a  >> $logfile
			break
		fi
	done
done


echo "getting all connected devices"  >> $logfile
# get devices currently connected to adb
export IFS=";"
adbdev=$($LOCALADB devices)
devices=$($LOCALADB devices|grep -v " device"|tr "\t" "\n"|grep -v "device"|tr "\n" ";")
devicelist=()
for device in $devices;do
	if [ -n "$device" ];
	then
		echo "found device X$device" >> $logfile
               devicelist+=(X$device)
	fi
done

# get removed devices
rmdev=()
for s in "${scr[@]}"
do :  
	flag=false
	for d in "${devicelist[@]}"
	do :  
		if [ "$d" == "$s" ];
		then
			echo "$d is still connected" >> $logfile
			flag=true
			break
		fi	
	done	
	if ! $flag
	then
		echo "$s was removed"  >> $logfile
		rmdev+=($s)
	fi	
done	

#for each device that was removed, kill the corresponding screen
for r in "${rmdev[@]}"
do :
	echo "killing screen for device $r" >> $logfile
	screen -X -S $r quit			
	
	
	dlen=${#r}
	dbkey=${r:1:$dlen}
	echo "updating device $dbkey" >> $logfile
	python $PYTHONCODE $dbkey 0 >> $logfile
done

