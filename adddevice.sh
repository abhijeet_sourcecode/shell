#!/bin/bash

source /home/test1080devicefarm/scripts/device.sh

# =========== Enter Path Names ================= #

logfile=
AWSIP=
AWSKEY=
LOCALADB=
AWSADB=
PYTHONCODE=
AWSNODE=
NODESCRIPT=
PATHTOCSV=
PATHTOMONITOR=
AWSSTARTSCRIPT=


# ============================================== #

timestamp(){
	date +"%s"
}


echo '==========================' >> $logfile

echo "device added: $1" >> $logfile

timestamp >> $logfile

for k in ${!allDevices[@]};do
	echo ${k} "-->" ${allDevices[${k}]}
done

echo "getting running screens"  >> $logfile
# get all running screens
scrcmd=$(ps auxw|grep -i screen|grep -v grep|awk '{print $15}')
$echo " screen "$scrcmd >> $logfile
allscr=()
export IFS=" "
while read -r line; do
	#echo "found line" $a >> $logfile
   	allscr+=("$line")
done <<< $scrcmd

# get the screens that are forwarding either one of the configured devices 
scr=()
for d in "${!allDevices[@]}" 
do : 
	for a in "${allscr[@]}" 
	do : 
		echo "compare ${d} with $a"
		if [ "$d" == "$a" ];
		then
			scr+=($a)
			echo "found screen" $a >> $logfile
			break
		fi
	done
done


echo "getting all connected devices"  >> $logfile
# get devices currently connected to adb
export IFS=";"
adbdev=$($LOCALADB devices)
devices=$($LOCALADB devices|grep -v " device"|tr "\t" "\n"|grep -v "device"|tr "\n" ";")
devicelist=()
for device in $devices;do
	if [ -n "$device" ];
	then
		echo "found device X$device"  >> $logfile
               devicelist+=(X$device)
	fi
done

# get new devices
newdev=()
for d in "${devicelist[@]}"
do :  
	flag=false
	for s in "${scr[@]}"
	do :  
		if [ "$d" == "$s" ];
		then
			echo "$d was already connected"  >> $logfile
			flag=true
			break
		fi	
	done	
	if ! $flag
	then
		echo "$d is a new device"  >> $logfile
		newdev+=($d)
	fi	
done	

runNode=false
echo "runnode $runNode"  >> $logfile
#for each new device, make a new tunnel and update the DB
for n in "${newdev[@]}"
do :
	echo "making new screen for device $n" >> $logfile
	screen -d -m -S $n >> $logfile
			
	echo "forwarding to port" >> $logfile
	#screen -S $n -X stuff "ssh -i $AWSKEY -N -R ${Ports[$n]}:localhost:${Ports[$n]} ubuntu@$AWSIP\n" >> $logfile
	screen -S $n -X stuff "autossh -M ${autoPorts[$n]} -i $AWSKEY -N -R ${Ports[$n]}:localhost:${Ports[$n]} ubuntu@$AWSIP\n" >> $logfile
	
	
	dlen=${#n}
	dbkey=${n:1:$dlen}
	echo "updating device $dbkey" >> $logfile
	python $PYTHONCODE $dbkey 1 >> $logfile

	runNode=true
done


#-------------------- starting node -----------------
if $runNode
then
	nodename="Node"
	nodename+=$(date -d "today" +"%Y%m%d%H%M%s") 

	echo "making a screen for $nodename" >> $logfile
	screen -d -m -S $nodename >> $logfile

	echo "starting node server on amazon" >> $logfile
	screen -S $nodename -X stuff "ssh -i $AWSKEY ubuntu@$AWSIP $AWSNODE $NODESCRIPT\n" >> $logfile
fi
